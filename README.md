Author: Antonio Silva Paucar
email: antonios@uoregon.edu

A simple webserver that shows html and css files located at the ./pages directory. The program will check if the file it is present in this folder and return the render of the html and css code. If it is not present, the server will send a 404 signal plus show a missing file page. If the file starts with "~", "/"(no considering the "/" already present) or "..", a 403 signal will be sent and forbidden page would be render in the web browser.

To start the server, we can type ./start.sh or make run. To stop, ./stop.sh or make stop. Between stopping and starting it again, it is suggested to wait around 30 secs because most of the times the port gets stucks and it doesn not allow creating a new connection using the 5000 port.
