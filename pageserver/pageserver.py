"""
  A trivial web server in Python.

  Based largely on https://docs.python.org/3.4/howto/sockets.html
  This trivial implementation is not robust:  We have omitted decent
  error handling and many other things to keep the illustration as simple
  as possible.

  It contains modifications that allow serving files located at "/pages".
"""

import config    # Configure from .ini files and command line
import logging   # Better than print statements
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)
# Logging level may be overridden by configuration

import socket    # Basic TCP/IP communication on the internet
import _thread   # Response computation runs concurrently with main program
import os#for source_path

def file_reader(file_name):
    """
    Based in spew.py provided by the instructor.

    The program gets the DOCROOT variable in credentials.ini by a global variable outside the scope and the source path by the file_name parameter that
    """
    DOCROOT = options.DOCROOT#options is a global variable that contains data from credential.ini
    source_path = os.path.join(DOCROOT, file_name)
    print(source_path)
    message = ""
    try:
        with open(source_path, 'r', encoding='utf-8') as source:
            for line in source:
                message+=line.strip()
    except OSError as error:
        message= "Error"#if error, then returns "Error" as a signal.

    return message#contains string with all the html and css code to be interpreted by the webbrowser
#no modify
def listen(portnum):
    """
    Create and listen to a server socket.
    Args:
       portnum: Integer in range 1024-65535; temporary use ports
           should be in range 49152-65535.
    Returns:
       A server socket, unless connection fails (e.g., because
       the port is already in use).
    """
    # Internet, streaming socket
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Bind to port and make accessible from anywhere that has our IP address
    serversocket.bind(('', portnum))
    serversocket.listen(1)    # A real server would have multiple listeners
    return serversocket
#no modify
def serve(sock, func):
    """
    Respond to connections on sock.
    Args:
       sock:  A server socket, already listening on some port.
       func:  a function that takes a client socket and does something with it
    Returns: nothing
    Effects:
        For each connection, func is called on a client socket connected
        to the connected client, running concurrently in its own thread.
    """
    while True:
        log.info("Attempting to accept a connection on {}".format(sock))
        (clientsocket, address) = sock.accept()
        _thread.start_new_thread(func, (clientsocket,))
##
# Starter version only serves cat pictures. In fact, only a
# particular cat picture.  This one.
##

def check_forbidden(text):
    '''
    checks if the request is for a forbidden section
    '''

    if "/.." in text or "//" in text or "/~" in text:
        print("forbidden")
        return 0# zero if forbidden
    else:
        print("Not forbidden")
        return 1#1 if it is a valid address.

def respond(sock):
    sent = 0
    request = sock.recv(1024)  # We accept only short requests
    request = str(request, encoding='utf-8', errors='strict')
    log.info("--- Received request ----")
    log.info("Request was {}\n***\n".format(request))
    parts = request.split()
    try:
        file_path = parts[1]
    except:
        file_path = ""
    estado = check_forbidden(file_path)#gets 0 or 1 depending if it is forbidden to check out.
    message = file_reader(file_path[1:])#gets the file that is looking for.

    if estado == 0:#the request contains .. , ~ or //
        message = file_reader("403.html")
        transmit(STATUS_FORBIDDEN , sock)
        transmit(message, sock)
    elif file_path == '/': #default, when no path is provided after 5000/
        print("/ index trivia.html")
        message = file_reader("trivia.html")
        transmit(STATUS_OK, sock)
        transmit(message, sock)
    elif  message == "Error":#when not found
        print("Not found")
        message = file_reader("404.html")
        transmit(STATUS_NOT_FOUND, sock)
        transmit(message, sock)
    elif len(parts) > 1 and parts[0] == "GET":#here, it reads the file required and send it to the client.
        print("Found!")
        #message = file_reader("trivia.html")
        message = file_reader(file_path[1:])
        transmit(STATUS_OK, sock)
        transmit(message, sock)
    else:
        log.info("Unhandled request: {}".format(request))
        transmit(STATUS_NOT_IMPLEMENTED, sock)
        transmit("\nI don't handle this request: {}\n".format(request), sock)

    sock.shutdown(socket.SHUT_RDWR)
    sock.close()
    return

def transmit(msg, sock):
    """It might take several sends to get the whole message out"""
    sent = 0
    while sent < len(msg):
        buff = bytes(msg[sent:], encoding="utf-8")
        sent += sock.send(buff)
###
#
# Run from command line
#
###
def get_options():
    """
    Options from command line or configuration file.
    Returns namespace object with option value for port
    """
    # Defaults from configuration files;
    #   on conflict, the last value read has precedence
    options = config.configuration()
    # We want: PORT, DOCROOT, possibly LOGGING

    if options.PORT <= 1000:
        log.warning(("Port {} selected. " +
                         " Ports 0..1000 are reserved \n" +
                         "by the operating system").format(options.port))

    return options
    
options = get_options()# get values of credentials.ini
#CODES for html
STATUS_OK = "HTTP/1.0 200 OK\n\n"
STATUS_FORBIDDEN = "HTTP/1.0 403 Forbidden\n\n"
STATUS_NOT_FOUND = "HTTP/1.0 404 Not Found\n\n"
STATUS_NOT_IMPLEMENTED = "HTTP/1.0 401 Not Implemented\n\n"

def main():
    port = options.PORT
    if options.DEBUG:
        log.setLevel(logging.DEBUG)
    sock = listen(port)
    log.info("Listening on port {}".format(port))
    log.info("Socket is {}".format(sock))
    serve(sock, respond)


if __name__ == "__main__":
    main()
